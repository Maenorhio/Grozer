import $ from "jquery";
import { LayersMap } from '../layers/LayersMap';

export class ThemeSwitcher
{
    constructor(map, layersMap, fetcher, wrkspace)
    {
        this.actualWrkspace = undefined;
        this.theme = undefined;
        this.map = map;
        this.layersMap = layersMap;
        this.fetcher = fetcher;
        this.wrkspace = wrkspace;

        this.initThemeSwitcher();
    }

    initThemeSwitcher()
    {
        this.fetcher.getThemes(this.wrkspace).then(fetcher =>
        {
            Object.keys(fetcher.layerHierarchy[this.wrkspace]).forEach(theme =>
            {
                $('div#theme-switcher-themes').append(
                    `<button class="btn btn-outline-secondary theme-btn theme" type="button" value="${this.wrkspace}-${theme}">${theme}</button>`);
            });
            $(".theme").click(this.getLayers(fetcher));
        });
    }

    getLayers(fetcher)
    {
        return (event) =>
        {
            const wrkspaceTheme = event.target.value;
            const [wrkspace, theme] = wrkspaceTheme.split('-');
            this.actualWrkspace = wrkspace;
            this.actualTheme = theme;

            fetcher.getLayers(wrkspace, theme).then(fetcher =>
            {
                // Clear all elements potentially previously loaded
                $('ul#layer-list').empty();
                $('ul#selected-layer-list').empty();
                this.layersMap.layerStack.stack.forEach(layer => { this.map.getLayers().remove(layer); });
                this.layersMap.clear();
                Object.keys(fetcher.layerHierarchy[wrkspace][theme]).forEach(layer =>
                {
                    $(`ul#layer-list`).append(`<li class="item list-group-item" id="${wrkspaceTheme}-${layer}">${layer}</eli>`);
                });
            });
        };
    }
}
