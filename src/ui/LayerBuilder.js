import GeoJSON from "ol/format/geojson";
import LayerVector from "ol/layer/vector";
import SourceVector from "ol/source/vector";
import Style from "ol/style/style";
import Stroke from "ol/style/stroke";
import Fill from "ol/style/fill";

let geoJsonFormat = new GeoJSON();

function getRandomFrom0To(num)
{
    return Math.floor(Math.random() * num);
}

export function createLayerWithRandomColor(layer)
{
    const color = `${getRandomFrom0To(256)}, ${getRandomFrom0To(256)}, ${getRandomFrom0To(256)}`;
    return new LayerVector({
        source: new SourceVector({
            features: geoJsonFormat.readFeatures(layer, { featureProjection : 'EPSG:3857' })
        }),
        style: new Style({
            stroke: new Stroke({
                color: `rgb(${color})`,
                width: 1
            }),
            fill: new Fill({
                color: `rgba(${color}, 0.5)`
            })
        })
    });
}
