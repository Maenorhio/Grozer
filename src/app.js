import $ from "jquery";
import Map from "ol/map";
import Tile from "ol/layer/tile";
import OSM from "ol/source/osm";
import View from "ol/view";
import LayerVector from "ol/layer/vector";
import SourceVector from "ol/source/vector";
import Style from "ol/style/style";
import Stroke from "ol/style/stroke";
import Fill from "ol/style/fill";
import Coordinate from "ol/coordinate";
import Proj from "ol/proj";
import Control from "ol/control";
import Scaleline from "ol/control/scaleline";
import Overlay from "ol/overlay";

// import "popper.js/dist/popper.min";
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/style.css';
import 'bootstrap/dist/js/bootstrap.min';
import 'ol/ol.css';

import { ThemeFetcher } from "./layers/ThemeFetcher";
import { LayersMap } from "./layers/LayersMap";
import { intersection } from "./spatial/intersection";
import { equalRange } from "./thematic/equalRange";
import { standardized } from "./thematic/standardized";

import { SortableLayer } from "./ui/SortableLayer";
import { ThemeSwitcher } from "./ui/ThemeSwitcher";

$(() =>
{
    var scaleLineControl = new Scaleline;
    // Displayed map
    let map = new Map({
        controls: Control.defaults({
            attributionOptions: {
                collapsible: false
            }
        }).extend([
            scaleLineControl
        ]),
        layers: [
            new Tile({
                source: new OSM()
            }),
        ],
        target: 'map',
        view: new View({
            center: [0, 0],
            zoom: 2
        })
    });

    // Contains information of displayed layers
    let layersMap = new LayersMap();

    // Fetch layers informations
    let fetcher = new ThemeFetcher();
    // Fetch workspaces and init the view with the TOPP workspace
    // TODO load the main workspace of the connected user
    fetcher.getWrkspaces().then(fetcher =>
    {
        new ThemeSwitcher(map, layersMap, fetcher, "topp");
    });

    $("#intersection").click(() =>
    {
        if (layersMap.containsKey('intersection'))
        {
            map.getLayers().remove(layersMap.intersection);
            layersMap.delete('intersection');
            // layersMap['intersection'] = undefined;
        }
        else
        {
            const layers = map.getLayers();
            const layersLength = layers.getLength();
            let intersectionLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            intersection(layers.item(layersLength -1).getSource().getFeatures(),
                         layers.item(layersLength -2).getSource().getFeatures())
                .forEach(inter => intersectionLayer.getSource().addFeature(inter));
            map.getLayers().push(intersectionLayer);
            layersMap.put('intersection', intersectionLayer);
        }
    });

    $("#equal-range").click(() =>
    {
        if (layersMap.containsKey('equalRange'))
        {
            map.getLayers().remove(layersMap.equalRange);
            // layersMap['equalRange'] = undefined;
            layersMap.delete('equalRange');
        }
        else
        {
            const layers = map.getLayers();
            const layersLength = layers.getLength();
            let layer = layers.item(layersLength -1);
            let features = layer.getSource().getFeatures();

            let equalRangeLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            //Coloring type: Monochromatic, Polychromatic, Manual
            equalRange(6, features, "PERIMETER", "Monochromatic")
                .forEach(ft => equalRangeLayer.getSource().addFeature(ft));
            map.getLayers().push(equalRangeLayer);
            layersMap.put('equalRange', equalRangeLayer);
        }
    });

    $("#standardized").click(() =>
    {
        if (layersMap.containsKey('standardized'))
        {
            map.getLayers().remove(layersMap.standardized);
            // layersMap['standardized'] = undefined;
            layersMap.drop('standardized');
        }
        else
        {
            const layers = map.getLayers();
            const layersLength = layers.getLength();
            let layer = layers.item(layersLength -1);
            let features = layer.getSource().getFeatures();

            let standardizedLayer = new LayerVector({
                source: new SourceVector(),
                style: new Style({
                    stroke: new Stroke({
                        color: 'green',
                        width: 1
                    }),
                    fill: new Fill({
                        color: 'rgba(0, 255, 0, 0.1)'
                    })
                })
            });

            //Coloring type: Monochromatic, Polychromatic, Manual
            standardized(6, features, "PERIMETER", "Monochromatic")
                .forEach(ft => standardizedLayer.getSource().addFeature(ft));
            map.getLayers().push(standardizedLayer);
            layersMap.put('standardized', standardizedLayer);
        }
    });


    // Initialise layer list
    let sortableLayers = new SortableLayer(map, layersMap, fetcher);

    $("#button-export-current-layers").on("click", function()
    {
        $("#popup-export-list-couche-selected").empty();
        $("#list-couche-selected").clone().appendTo("#popup-export-list-couche-selected");
    });


    var container = document.getElementById('popup');
    // var content = document.getElementById('popup-content');
    // var closer = document.getElementById('popup-closer');


    var popup = new Overlay({
        element: container
    });

    map.addOverlay(popup);

    map.on('click', function(evt) {

        var coordinate = evt.coordinate;
        var hdms = Coordinate.toStringHDMS(Proj.transform(
            coordinate, 'EPSG:3857', 'EPSG:4326'));

        $(container).popover('dispose');
        popup.setPosition(coordinate);
        // the keys are quoted to prevent renaming in ADVANCED mode.


        displayFeatureInfo(evt.pixel);

    });


    var displayFeatureInfo = function(pixel)
    {
        var features = [];
        var contenu = '';
        let compteur = 0;


        map.forEachFeatureAtPixel(pixel, function(feature, layer)
        {
            features.push(feature);
        });
        if (features.length > 0)
        {

            var info = [];
            for (var i = 0, ii = features.length; i < ii; ++i)
            {
                info.push(features[i].values_);

                console.log(info);
                console.log(features[i]);
            }


            info.forEach(function(element)
            {
                console.log(element);
                contenu+='<div class=<p> ID: ' + features[compteur].id_ + "</p>";
                contenu+= "<hr>";
                for(var key in element)
                {
                    if(key!='geometry')
                        contenu += "<p>" + key + ": "+ element[key] + "</p>";
                }
                contenu+= "<hr>";

                compteur++;
            });

            $(container).popover
            ({
                'placement': 'top',
                'animation': false,
                'html': true,
                'content': contenu
            });
            $(container).popover('show');

        }
    };

});
