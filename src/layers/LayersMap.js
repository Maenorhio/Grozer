import { LayerStack } from "./LayerStack";

export class LayersMap
{
    constructor()
    {
        this.map = {};
        this.stack = new LayerStack();
    }

    get layerStack()
    {
        return this.stack;
    }

    put(key, /** ol.layer.Layer */ value)
    {
        this.putAtIndex(key, value, 0);
    }

    get(key)
    {
        return this.map[key];
    }

    putAtIndex(key, /** ol.layer.Layer */ layer, /** Int */ index)
    {
        this.map[key] = layer;
        this.stack.insertAtIndex(index, layer);
    }

    move(/** Int */ oldIndex, /** Int */ newIndex)
    {
        this.stack.move(oldIndex, newIndex);
    }

    delete(key)
    {
        const layer = this.map[key];
        this.map[key] = undefined;
        for (let i = 0; i < this.stack.stack.length; ++i)
        {
            if (this.stack.stack[i] === layer)
            {
                this.stack.dropFromIndex(i);
                break;
            }
        }
    }

    containsKey(key)
    {
        return key in this.map;
    }

    clear()
    {
        this.map = {};
        this.stack.clear();
    }
}
