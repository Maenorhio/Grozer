import $ from "jquery";
import { BASE_URL, REST_URL } from "../const";

export class ThemeFetcher
{
    constructor()
    {
        this.hierarchy = { };
    }

    get layerHierarchy()
    {
        return this.hierarchy;
    }

    get wrkspaces() { return Object.keys(this.hierarchy); }

    getWrkspaces()
    {
        return new Promise(success =>
        {
            $.get(`${REST_URL}/workspaces.json`, response =>
            {
                response.workspaces.workspace
                    .forEach(wrkspace => this.hierarchy[wrkspace.name] = {});
                success(this);
            });
        });
    }

    getThemes(wrkspaceName)
    {
        return new Promise((success, failure) =>
        {
            if (this.wrkspaces.indexOf(wrkspaceName) === -1)
                failure(null);
            $.get(`${REST_URL}/workspaces/${wrkspaceName}/datastores.json`, response =>
            {
                /// TODO check if response !== { "dataStores": "" }
                response.dataStores.dataStore
                    .forEach(theme => this.hierarchy[wrkspaceName][theme.name] = {});
                success(this);
            });
        });
    }

    getLayers(wrkspaceName, themeName)
    {
        return new Promise((success, failure) =>
        {
            if (this.wrkspaces.indexOf(wrkspaceName) === -1 || !(themeName in this.hierarchy[wrkspaceName]))
                failure();
            $.get(`${REST_URL}/workspaces/${wrkspaceName}/datastores/${themeName}/featuretypes.json`, response =>
            {
                response.featureTypes.featureType
                    .forEach(layer => this.hierarchy[wrkspaceName][themeName][layer.name] = {});
                success(this);
            });
        });
    }

    getLayer(wrkspaceName, themeName, layerName)
    {
        return new Promise((success, failure) =>
        {
            if (!(wrkspaceName in this.hierarchy) || !(themeName in this.hierarchy[wrkspaceName]) ||
                !(layerName in this.hierarchy[wrkspaceName][themeName]))
                failure();
            $.get(`${BASE_URL}/${wrkspaceName}/ows?service=WFS&version=1.0.0&request=GetFeature&typeName=${wrkspaceName}:${layerName}&` +
                  `outputFormat=application%2Fjson`,
                  (response) =>
            {
                this.hierarchy[wrkspaceName][themeName][layerName] = response;
                success(this);
            });

        });
    }
}
