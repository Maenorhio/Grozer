export function colorFeaturesMonochromatic(classesNumber, negativeValues)
{
    //Red; Orange; Yellow; Green; Blue; Purple; Pink;
    let HValues = [0, 30, 60, 120, 240, 280, 300];
    let colors = [];
    //First acceptable value for L: 90 (lighter); Last: 10 (darker); >90 is too white; <10 is too black
    const firstLValue = 85;
    const lastLValue = 15;
    //H (hue) is the color chosen. 0 is red up to 360 which comes full circle so also red.
    let H = 0;
    if (negativeValues)
    {
        H = 240;
    }
    else
    {
        H = 0;
    }
    //S (saturation) could always be 100, changing it is not necessary to get a good range of colors
    const S = 100;
    //L (lightness) dictates the lightness of the color of the first class
    let L = firstLValue;
    //LRange is the difference in L of two colors
    const LRange = Math.floor((firstLValue - lastLValue) / (classesNumber-1));
    for (let i = 0; i < classesNumber; i++)
    {
        let strokeColor = "hsl("+H+", "+S+"%, "+L+"%)";
        let fillColor = "hsla("+H+", "+S+"%, "+L+"%, 0.3)";
        L = L - LRange;
        colors.push(strokeColor);
        colors.push(fillColor);
    }
    return colors;
}

export function colorFeaturesPolychromatic(classesNumber, negativeValues)
{
    //Red; Orange; Yellow; Green; Blue; Purple; Pink;
    let HValues = [0, 30, 60, 120, 240, 280, 300];
	let colors = [];
    let firstHValue = 0;
    let lastHValue = 0;
	//H (hue) is the color chosen. 0 is red up to 360 which comes full circle so also red.
	if (negativeValues)
    {
        firstHValue = 240;
        lastHValue = 120;
    }
    else
    {
        firstHValue = 60;
        lastHValue = 0;
    }
    let H = firstHValue;
	//S (saturation) could always be 100, changing it is not necessary to get a good range of colors
	const S = 100;
	//L (lightness) dictates the lightness of the color of the first class
	const L = 50;
	//HRange is the difference in H of two colors
	const HRange = Math.floor((firstHValue - lastHValue) / (classesNumber-1));
	for (let i = 0; i < classesNumber; i++)
	{
		let strokeColor = "hsl("+H+", "+S+"%, "+L+"%)";
		let fillColor = "hsla("+H+", "+S+"%, "+L+"%, 0.3)";
		H = H - HRange;
		colors.push(strokeColor);
		colors.push(fillColor);
	}
	return colors;
}

export function colorFeaturesManual(classesNumber)
{

}

export function quickSortFeaturesByValue(items, sortValue, left, right)
{
    var index;
    if (items.length > 1)
    {

        left = typeof left != "number" ? 0 : left;
        right = typeof right != "number" ? items.length - 1 : right;

        index = partition(items, left, right, sortValue);

        if (left < index - 1)
        {
            quickSortFeaturesByValue(items, sortValue, left, index - 1);
        }

        if (index < right)
        {
            quickSortFeaturesByValue(items, sortValue, index, right);
        }
    }

    return items;
}

function partition(items, left, right, sortValue)
{
    var pivot   = items[Math.floor((right + left) / 2)].values_[sortValue],
        i       = left,
        j       = right;

    while (i <= j)
    {
        while (items[i].values_[sortValue] < pivot)
        {
            i++;
        }

        while (items[j].values_[sortValue] > pivot)
        {
            j--;
        }

        if (i <= j)
        {
            swap(items, i, j);
            i++;
            j--;
        }
    }

    return i;
}

function swap(items, firstIndex, secondIndex)
{
    var temp = items[firstIndex];
    items[firstIndex] = items[secondIndex];
    items[secondIndex] = temp;
}
