import jStat from "jStat";

import Style from "ol/style/style";
import Stroke from "ol/style/stroke";
import Fill from "ol/style/fill";

import { colorFeaturesMonochromatic } from "./featureUtils";
import { colorFeaturesPolychromatic } from "./featureUtils";
import { colorFeaturesManual } from "./featureUtils";
import { quickSortFeaturesByValue } from "./featureUtils";

export function standardized(classesNumber, features, sortValue, coloringType)
{
	let sortedFeatures = quickSortFeaturesByValue(features, sortValue);

	let minSortValue = sortedFeatures[0].values_[sortValue];

	//Coloring type: Monochromatic, Polychromatic, Manual
	if (coloringType === "Monochromatic")
	{
		if (minSortValue > 0)
		{
			var featuresColors = colorFeaturesMonochromatic(classesNumber, false);
		}
		else
		{
			var featuresColors = colorFeaturesMonochromatic(classesNumber, true);
		}
	}
	else if (coloringType === "Polychromatic")
	{
		if (minSortValue > 0)
		{
			var featuresColors = colorFeaturesPolychromatic(classesNumber, false);
		}
		else
		{
			var featuresColors = colorFeaturesPolychromatic(classesNumber, true);
		}
	}
	else
	{
		var featuresColors = coloringType;
	}

	//Create and sort classes
	let sortedFeaturesValues = [];
	for (let i = 0; i < sortedFeatures.length; i++)
	{
		sortedFeaturesValues.push(sortedFeatures[i].values_[sortValue]);
	}

	let mean = Math.ceil(jStat.mean(sortedFeaturesValues));
	let stdev = Math.ceil(jStat.stdev(sortedFeaturesValues));
	let lastUpValue = mean + stdev;
	let lastDownValue = mean - stdev;
	let classesValues = [[lastDownValue, mean], [mean, lastUpValue]];

	for (let i = 0; i < classesNumber-2; i++)
	{
		if (i%2 == 0)
		{
			let down = lastUpValue;
			lastUpValue = lastUpValue + stdev;
			classesValues.push([down, lastUpValue]);
		}
		else
		{
			let up = lastDownValue;
			lastDownValue = lastDownValue - stdev;
			classesValues.unshift([lastDownValue, up]);
		}
	}

	//Organize features between classes
	let currentClassIndex = 0;
	let currentClassValue = classesValues[currentClassIndex];
	let currentClassFeatures = [];
	let sortedClasses = [];

	for (let i = 0; i < sortedFeatures.length; i++)
	{
		if (sortedFeatures[i].values_[sortValue] <= currentClassValue[1])
		{
			currentClassFeatures.push(sortedFeatures[i]);
		}
		else
		{
			while (sortedFeatures[i].values_[sortValue] > currentClassValue[1])
			{
				currentClassIndex += 1;
				currentClassValue = classesValues[currentClassIndex];
				sortedClasses.push(currentClassFeatures);
				currentClassFeatures = [];
			}
			currentClassFeatures.push(sortedFeatures[i]);
		}
		if (i === sortedFeatures.length-1)
		{
			sortedClasses.push(currentClassFeatures);
		}
	}

	let k = 0;
	for (let i = 0; i < sortedClasses.length; i++)
	{
		let strokeColor = featuresColors[k];
		let fillColor = featuresColors[k+1];
		k = k + 2;

		for (let j = 0; j < sortedClasses[i].length; j++)
		{
			sortedClasses[i][j].setStyle(new Style({
	            stroke: new Stroke({
	                color: strokeColor,
	                width: 1
	            }),
	            fill: new Fill({
	                color: fillColor
	            })
	        }));
		}
	}
}
