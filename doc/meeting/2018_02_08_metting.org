#+TITLE: Projet industriel \linebreak \small{\it{Compte rendu de la réunion - 08/02/2018}}
#+OPTIONS: author:nil date:nil toc:nil

* Points abordés
   - L’analyse thématique est le point le plus important de l’application
   - La création de thèmes est le point suivant à développer
   - L’analyse spatiale a posé des problèmes techniques:
     - Nous avons opté pour l’utilisation d’une bibliothèque JS
     - La méthode de départ avec PostGIS doit être explicitée dans le rapport
   - Le rapport final comportera une partie besoins non fonctionnels où nous expliquerons les différents choix d’architecture qui s’offraient à nous et nous ferons un pour et contre de chacun, en justifiant nos choix
   - Ce rapport fera partie du produit logiciel, c’est à dire la livraison de l’application et la définition des prospections

* Objectifs
   - Laisser la première version de l’analyse spatiale pour réaliser en priorité l’analyse thématique
   - Démarrer le développement de la première version de l’analyse thématique
   - Réaliser une première version de l’interface graphique
